resource "aws_dynamodb_table" "dynamodb_lock" {
  name           = "LockStatus"
  read_capacity  = 5
  write_capacity = 5
  hash_key       = "LockID"

  attribute {
    name = "LockID"
    type = "S"
  }
}