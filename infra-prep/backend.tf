terraform {
  required_version = ">= 1.1"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
  backend "local" {
    path = "infra-prep.tfstate"
  }
}

provider "aws" {
  region = "us-east-1"
}
