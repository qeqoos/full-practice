AWS

^ TF config

3-tier architecture

Variants:
- ec2
- ecs
- eks
- beanstalk (G2K)

Container in ecr

Static content in S3 + Cloudfront

State in S3, DynamoDB lock

---

Q:
Soft:
- Что такое DevOps
DevOps - набор практик, инструментов, подходов к разработке, которые нацелены на улучшение надёжности, ,взаимодействия членов команд, скорости разработки, безопасности, доставки програмного продукта, а так же снижение финансовых затрат.

- CI/CD
CI - непрерывная интеграция. Заключается в совершении частых, постоянных изменений и отправке их в целевую ветку разработки, для получения быстрого фидбека об успешности изменений путём автоматического запуска различных проверок, тестов и сборки готового артефакта приложения. Если новые изменения не прошли проверку, разработчик получает отчёт о том, что и почему не заработало, какие тесты не прошли. В итоге мы постоянно получаем новые версии приложения, в целевую ветку попадает только проверенный и протестированный код, и мы всегда имеем рабочий артефакт.

CD - непрерывная доставка/развёртывание. Каким образом готовый артефакт из предыдущей стадии будет доставлен на целевое окружение. Будет ли это сделано автоматически (deployment) либо с мануальным подтверждением от человека (delivery)

- Kanban/scrum/agile, какие главные артефакты методологий
Agile-принципы лежат в основе scrum и kanban. Agile - набор «гибких» подходов к разработке программного обеспечения.

Scrum - спринты (итерации разработки, 1 неделя - 1 месяц), беклог, дедлайны, ретроспекшн, daily standup, скрам-мастер, владелец продукта, MVP (minimum valuable product). Перед спринтом формулируются задачи на данный спринт, в конце – обсуждаются  результаты, а команда начинает новый спринт.

Kanban - борда, разделена на беклог (to-do), в процессе, done.И по одной задаче вытягивается и делается. После завершения задачи берётся следующая и тд. Нет ролей владельца продукта и scrum-мастера. Бизнес-процесс делится  на стадии выполнения конкретных задач: «Планируется», «Разрабатывается», «Тестируется», «Завершено» и др.

Главный показатель эффективности в kanban – это среднее время прохождения задачи по доске. Задача прошла быстро – команда работала продуктивно и слаженно. Задача затянулась – надо думать, на каком этапе и почему возникли задержки и чью работу надо оптимизировать.

- Почему ушли от waterflow к scrum
Потому что в ватерфоле в предыдущим стадиям не возвращались, из-за этого процесс разработки не был гибким. Он следовал чёткому плану без возвожности внедрить что-то в процессе, отредактировать и т.д.

- Оценивание сложности таска
На основе своего опыта (по аналогии).
На основе опыта коллег, других людей.
Разбиение на мелкие таски и уже их оценивание.
Покер планирования.

- Ты пришел в проект. Есть 5 задач. К кому ты пойдешь первым для их решения? 
К лиду, затем за более мелкими вопросами к коллегам, документации.

- Квадрат с срочно/важно/не срочно/не важно. 


- Shared responsibility model
В клауде, зона ответственностей клиетна и провайдера разелена. По-сути, провайдер отвечает за безопасность КЛАУДА, а пользователь за безопасность В КЛАУДЕ.

Customer is responsible for security IN the cloud:
- Customer data
- Platform, application, and IAM
- OS patching 
- Antivirus
- Network, and firewall configuration
- Multi-factor authentication
- Password and key rotation
- Security groups
- Resource-based policies
- VPC
- Data in transit and at rest
 
AWS is responsible for security OF the cloud:
- Regions, availability zones, and edge locations
- Physical server level and below
- Storage device decommissioning according to industry standards
- Personnel security
- EC2 instances and spoofing protection (ingress/egress filtering)
- Port scanning against rules even if it’s your own environment
- Instances on the same physical device are separated at the hypervisor
level; they are independent of each other
- Underlying OS patching on Lambda, RDS, DynamoDB, and other managed
services; customer focuses on security
 
Shared Controls – Controls which apply to both the infrastructure layer and customer layers, but in completely separate contexts or perspectives. Examples include:
* Patch Management – AWS is responsible for patching and fixing flaws within the infrastructure, but customers are responsible for patching their guest OS and applications.
* Configuration Management – AWS maintains the configuration of its infrastructure devices, but a customer is responsible for configuring their own guest operating systems, databases, and applications.
* Awareness & Training - AWS trains AWS employees, but a customer must train their own employees.

- SDLC (Software Development Lifecycle) stages.
1. Plan
2. Analyze
3. Develop
4. Test
5. Deploy

- DevOps stages.
1. Plan
2. Develop
3. Build
4. Test
5. Deploy
6. Release
7. Monitor
8. Continious feedback

- Какие части SDLC покрывает CI/CD, где он начинается/заканчивается. 
Develop, build, test, deploy (maybe release).

- Deployment strategies
Canary
Blue-green
Shadow
Rolling

- В чем разница между джуном мидлом и сеньйором? 
Джун - человек без опыта, или с небольшим опытом. Он знает инструменты для работы, как их использовать по назначению, имеет представление о разных концептах в его сфере. За ним нужно присматривать, направлять в нужное русло, если он делает что-то не так. Он не общается с заказчиком, часто задаёт вопросы.

Между джуном и мидлом в том, что мидл в отличии от джуна работает сам, ему реже приходится приходить с вопросами к коллегам, людям по-сеньернее. Так же у него есть багаж опыта, у него есть алгоритм действий если он натыкается на проблему или новую технологию. Он может общатся с заказчиком, имеет лучше понимание бизнесс-процессов.

Между мидлом и сеньером в уровне софтскиллов (даже не обязательно, что хардскиллов, но скорее всего и ими тоже), умением менеджить людей в команде. Сеньер готов решать проблемы, которые никто не знает как реашть, кроме него. Знание инфраструры, лучших практик, количество набитых шишек, и общее понимание всего. Он так же выступает инициатором решений, более основательно.

Hard:
- Самые большие челенджи? (General)
nosa (huge service, canary deployment, automatic route53 switch, no possibility to test prod env, golang, ssl removal)
Apigee to Kong migration switch 
Services with multiple envs, custom jobs
Create API for golang for readiness probes
Terraform deploy K8S resources into multiple clusters
Troubleshooting dependency errors
Learn about Kafka
Разобраться в пайплайне проекта и внедрить необходимые изменения, ничего не сломая при этом.
Выбивание доступов в Кубер и АВС 

- Где начинается/заканчивается CI и CD? (General)
С момента как разработчик написал код и залил его в систему котроля версий и до получения готового артефакта приложения (СІ), либо до доставки его на целевое окружение (CD)

- Идемпотентность (General)
Свойство кода или операции отдавать одинаковый ожидаемый результат при многократных (любое кол-во) его вызовах. Позволяет достичь защиты от дубликации, перезаписи, ненужных операций, стабильности в ожиданиях.

- git operations from CLI (Git)
Замена remote на fatch и на push (git remote set)
Reset (soft, hard)
--amend
git diff
git checkout -b
git cherry-pick
git pull

- Что такое origin в git clone origin/master (Git)
origin - alias, что указывает на удалённый репозиторий в текущей директории. Можно подставить другой, оттуда и будут спулены изменения.

- Branching strategies (Git)
GitFlow
GithubFlow
GitlabFlow
TrunkFlow
Branching

- Provisioner (Terraform)
Позволяет запускать команды после создания ресурса. 
* local-exec - запуск команд с машины, на которой запускался terraform
* remote-exec - запуск команд на удалённом ресурсе
* file - передача файла на ресурс с машины, которая запускает terraform

- Граф зависимостей (Terraform)
terraform строит граф зависимостей перед каждой командой, и потом проходится по нему и осуществляет plan, apply и т.д. Сам терраформ может определять зависимости используя блок depends_on. Все остальные механизмы выстраивания зависимостей возлагаются на провайдера (самое распространённое - референсы ресурсов).

- Terraform workspace (Terraform) 
Позволяет организовать несколько енвов, при условии если конфигурация для них одинаковая. Все стейты находится в одном state-файле. Между воркспейсами можно переключаться.

- Terraform plan что делает, какие процессы происходят (Terraform)
Строит граф зависимостей и генерирует конфигурацию, считывает текущий стейт, сравнивает два состояния и выводит изменения, которые будут применены при  apply.

- Как поменять user-data без опускания инстанса (AWS + Terraform)
lifecycle {
  ignore_changes = [user-data]
}

- ALB vs NLB (AWS)
ALB - lvl7 OSI, NLB - lvl4

- NACL vs SG (AWS)

- USER vs GROUP vs ROLE (AWS)
- Сборщики образов для AWS (AWS)
- Разница между балансерами в AWS (application and network) (AWS)
- Как организован доступ к S3 bucket? (access policy, public/private access) (AWS)
- Группы доступа в IAM (AWS)

- Какие инструменты для просмотра параметров машины (Linux)
- Приходит клиент и говорит, что сервер не отвечает. Твои действия? (Troubleshooting)

- Надо создать две группы ресурсов - как организуете? Две сети, две подсети, разные сети, одна сеть и тд (AWS)
