#!/bin/bash

yum update -y
amazon-linux-extras install nginx1 -y

export SERVER_PRIVATE_IP=$(curl http://169.254.169.254/latest/meta-data/local-ipv4)

cat <<EOF > /usr/share/nginx/html/index.html
<html>
<head>
    <title>Top page in da wrld</title>
</head>
<body>
    <h1>Hello from ${SERVER_PRIVATE_IP} to your sister and your cousin</h1>
    <p>Feeling a little goofy rn</p>
</body>
</html>
EOF

systemctl start nginx.service