variable "server_open_ports" {
  type    = list(any)
  default = [22, 80]
}

variable "lb_open_ports" {
  type    = list(any)
  default = [80, 443]
}

variable "protocol_map" {
  type = map(string)
  default = {
    "22"  = "ssh"
    "80"  = "http"
    "443" = "https"
  }
}

variable "hosted_zone" {
  type        = string
  default     = "cmcloudlab695.info"
  description = "Public hosted zone for subdomains"
}

