resource "aws_security_group" "web_sg" {
  name        = "web"
  description = "Allow web traffic"
  vpc_id      = module.vpc.vpc_id

  dynamic "ingress" {
    for_each = var.server_open_ports
    content {
      description     = "Allow ${var.protocol_map[ingress.value]}"
      from_port       = ingress.value
      to_port         = ingress.value
      protocol        = "tcp"
      cidr_blocks     = ["0.0.0.0/0"]
      security_groups = [aws_security_group.alb_sg.id]
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_key_pair" "access_key" {
  key_name   = "access-key"
  public_key = file("~/.ssh/id_rsa.pub")
}

data "aws_ami" "amazon_linux" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-kernel-5.10-hvm-2.0*"]
  }

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

resource "aws_launch_template" "server_template" {
  name_prefix            = "web-template"
  image_id               = data.aws_ami.amazon_linux.id
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.web_sg.id]
  key_name               = aws_key_pair.access_key.key_name
  update_default_version = true
  # iam_instance_profile {
  #   name = "test"
  # }
  user_data = filebase64("./user-data.sh")
  # lifecycle {
  #   ignore_changes = [user_data]
  # }
}

resource "aws_autoscaling_group" "web_asg" {
  name                      = "web-asg"
  vpc_zone_identifier       = module.vpc.private_subnets
  desired_capacity          = 2
  max_size                  = 4
  min_size                  = 2
  default_cooldown          = 180
  health_check_grace_period = 120
  health_check_type         = "ELB"
  termination_policies      = ["OldestLaunchTemplate", "OldestInstance"]
  target_group_arns         = [aws_lb_target_group.web_tg.arn]
  launch_template {
    id      = aws_launch_template.server_template.id
    version = "$Latest"
  }

  instance_refresh {
    strategy = "Rolling"
    preferences {
      min_healthy_percentage = 50
    }
  }
}

resource "aws_autoscaling_policy" "webapp_scale_up" {
  name                    = "Web-scale-policy-up"
  enabled                 = true
  autoscaling_group_name  = aws_autoscaling_group.web_asg.id
  policy_type             = "StepScaling"
  adjustment_type         = "ChangeInCapacity"
  metric_aggregation_type = "Average"

  step_adjustment {
    scaling_adjustment          = 1
    metric_interval_lower_bound = 0
  }
}

resource "aws_autoscaling_policy" "webapp_scale_down" {
  name                    = "Web-scale-policy-down"
  enabled                 = true
  autoscaling_group_name  = aws_autoscaling_group.web_asg.id
  policy_type             = "StepScaling"
  adjustment_type         = "ChangeInCapacity"
  metric_aggregation_type = "Average"

  step_adjustment {
    scaling_adjustment          = -1
    metric_interval_upper_bound = 0
  }
}

resource "aws_cloudwatch_metric_alarm" "scale_up" {
  alarm_name          = "Web-scale-up-alarm"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = 1
  metric_name         = "RequestCountPerTarget"
  namespace           = "AWS/ApplicationELB"
  period              = 60
  statistic           = "Sum"
  threshold           = 20
  alarm_description   = "This metric monitors number of requests going up"
  alarm_actions       = [aws_autoscaling_policy.webapp_scale_up.arn]
  dimensions = {
    TargetGroup  = aws_lb_target_group.web_tg.arn_suffix
    LoadBalancer = aws_lb.web_alb.arn_suffix
  }
}

resource "aws_cloudwatch_metric_alarm" "scale_down" {
  alarm_name          = "Web-scale-down-alarm"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = 1
  metric_name         = "RequestCountPerTarget"
  namespace           = "AWS/ApplicationELB"
  period              = 60
  statistic           = "Sum"
  threshold           = 20
  alarm_description   = "This metric monitors number of requests going down"
  alarm_actions       = [aws_autoscaling_policy.webapp_scale_down.arn]
  dimensions = {
    TargetGroup  = aws_lb_target_group.web_tg.arn_suffix
    LoadBalancer = aws_lb.web_alb.arn_suffix
  }
}

resource "aws_autoscaling_attachment" "asg_attachment_alb" {
  autoscaling_group_name = aws_autoscaling_group.web_asg.id
  lb_target_group_arn    = aws_lb_target_group.web_tg.arn
}
