output "public_lb_dns" {
  value       = aws_lb.web_alb.dns_name
  description = "External link to balancer"
  depends_on  = [aws_lb.web_alb]
}

output "site_url" {
  value       = aws_route53_record.r53record.fqdn
  description = "Site URL with r53"
  depends_on  = [aws_route53_record.r53record]
}
