terraform {
  required_version = ">= 1.1"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
  backend "s3" {
    bucket         = "terraform-state-bucket-69"
    key            = "infra.tfstate"
    region         = "us-east-1"
    dynamodb_table = "LockStatus"
  }
}

provider "aws" {
  region = "us-east-1"
}
